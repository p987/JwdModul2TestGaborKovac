DROP SCHEMA IF EXISTS salon;
CREATE SCHEMA salon DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE salon;

CREATE TABLE frizeri (
	id INT AUTO_INCREMENT, 
	ime VARCHAR(255) NOT NULL, 
	cena_sisanja FLOAT NOT NULL, 
    radni_dani VARCHAR(255) NOT NULL, 
	smena INT NOT NULL, 
	PRIMARY KEY(id)
);

CREATE TABLE termini (
	id BIGINT AUTO_INCREMENT, 
    frizer_id INT NOT NULL,
	datum_i_vreme DATETIME NOT NULL,
    ime VARCHAR(255) NOT NULL, 
    e_mail VARCHAR(255) NOT NULL,
	telefon VARCHAR(255) NOT NULL,
	PRIMARY KEY(id), 
	FOREIGN KEY(frizer_id) REFERENCES frizeri(id)
);

INSERT INTO frizeri (id, ime, cena_sisanja, radni_dani, smena) VALUES (1, 'Frizer 1', 600.0, 'MONDAY,WEDNESDAY,FRIDAY', 1);
INSERT INTO frizeri (id, ime, cena_sisanja, radni_dani, smena) VALUES (2, 'Frizer 2', 600.0, 'MONDAY,WEDNESDAY,FRIDAY', 2);
INSERT INTO frizeri (id, ime, cena_sisanja, radni_dani, smena) VALUES (3, 'Frizer 3', 700.0, 'TUESDAY,THURSDAY,SATURDAY', 1);
INSERT INTO frizeri (id, ime, cena_sisanja, radni_dani, smena) VALUES (4, 'Frizer 4', 700.0, 'TUESDAY,THURSDAY,SATURDAY', 2);

INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (1, '2023-11-01 09:00', 'Aaa', 'a@a.com', '+381611111111');
INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (2, '2023-11-01 14:00', 'Bbb', 'b@b.com', '+381622222222');
INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (1, '2023-11-01 09:30', 'Ccc', 'c@c.com', '+381633333333');
INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (2, '2023-11-01 14:30', 'Ddd', 'd@d.com', '+381644444444');
INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (3, '2023-11-02 09:00', 'Eee', 'e@e.com', '+381655555555');
INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (4, '2023-11-02 14:00', 'Fff', 'f@f.com', '+381666666666');
INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (3, '2023-11-02 09:30', 'Ggg', 'g@g.com', '+381677777777');
INSERT INTO termini (frizer_id, datum_i_vreme, ime, e_mail, telefon) VALUES (4, '2023-11-02 14:30', 'Hhh', 'h@h.com', '+381688888888');
