package frizerskiSalon.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Termini {
	private long id;
	private Frizeri frizer;
	private LocalDateTime datumIVreme;
	private String ime;
	private String eMail;
	private String telefon;


	public Termini(long id, Frizeri frizer, LocalDateTime datumIVreme, String ime, String eMail, String telefon) {
		super();
		this.id = id;
		this.frizer = frizer;
		this.datumIVreme = datumIVreme;
		this.ime = ime;
		this.eMail = eMail;
		this.telefon = telefon;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Frizeri getFrizer() {
		return frizer;
	}


	public void setFrizer(Frizeri frizer) {
		this.frizer = frizer;
	}


	public LocalDateTime getDatumIVreme() {
		return datumIVreme;
	}


	public void setDatumIVreme(LocalDateTime datumIVreme) {
		this.datumIVreme = datumIVreme;
	}


	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public String geteMail() {
		return eMail;
	}


	public void seteMail(String eMail) {
		this.eMail = eMail;
	}


	public String getTelefon() {
		return telefon;
	}


	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Termini other = (Termini) obj;
		return id == other.id;
	}


	@Override
	public String toString() {
		return "Termini [id=" + id + ", frizer=" + frizer + ", datumIVreme=" + datumIVreme + ", ime=" + ime + ", eMail="
				+ eMail + ", telefon=" + telefon + "]";
	}
	
	
}
