package frizerskiSalon.listeners;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;

import frizerskiSalon.controller.TerminiController;

@Component
public class InitHttpSessionListener implements HttpSessionListener{
	public void sessionCreated(HttpSessionEvent event) {
		System.out.println("Inicijalizacija sesisje HttpSessionListener...");
		
		//nece da inicijalizuje sesiju
		Double iznos = 1000.0;//zadatak4
		event.getSession().setAttribute(TerminiController.KORISNIK_KEY, iznos);
		System.out.println(event.getSession().getAttribute(TerminiController.KORISNIK_KEY));
		System.out.println("Uspeh HttpSessionListener!");
	}
	
	public void sessionDestroyed(HttpSessionEvent event) {
		System.out.println("Brisanje sesisje HttpSessionListener...");
		
		System.out.println("Uspeh HttpSessionListener!");
	}	
}
