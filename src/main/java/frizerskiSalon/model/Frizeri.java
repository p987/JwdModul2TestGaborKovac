package frizerskiSalon.model;

import java.util.Objects;

public class Frizeri {
	private int id;
	private String ime;
	private double cenaSisanja;
	private String radniDani;
	private int smena;
	
	
	public Frizeri(int id, String ime, double cenaSisanja, String radniDani, int smena) {
		super();
		this.id = id;
		this.ime = ime;
		this.cenaSisanja = cenaSisanja;
		this.radniDani = radniDani;
		this.smena = smena;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public double getCenaSisanja() {
		return cenaSisanja;
	}


	public void setCenaSisanja(double cenaSisanja) {
		this.cenaSisanja = cenaSisanja;
	}


	public String getRadniDani() {
		return radniDani;
	}


	public void setRadniDani(String radniDani) {
		this.radniDani = radniDani;
	}


	public int getSmena() {
		return smena;
	}


	public void setSmena(int smena) {
		this.smena = smena;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frizeri other = (Frizeri) obj;
		return id == other.id;
	}


	@Override
	public String toString() {
		return "frizeri [id=" + id + ", ime=" + ime + ", cenaSisanja=" + cenaSisanja + ", radniDani=" + radniDani
				+ ", smena=" + smena + "]";
	}
	
	
}
