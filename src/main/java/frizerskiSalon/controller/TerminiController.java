package frizerskiSalon.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import frizerskiSalon.model.Frizeri;
import frizerskiSalon.model.Termini;

@Controller
@RequestMapping(value="/Termini")
public class TerminiController {
	public static final String TERMINI_KEY = "termini";
	public static final String FRIZERI_KEY = "frizeri";
	public static final String KORISNIK_KEY = "korisnik";
	public static DateTimeFormatter FORMATER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	
	
	@Autowired
	private ServletContext servletContext;
	
//	@Autowired
//	private ParkingKarteService parkingKarteService;
	
	
	
	@PostMapping("/Create")
	public String zadatak2(
			@RequestParam(required = false) String frizerId,
			@RequestParam(required = false) String datumIVreme,
			@RequestParam(required = false) String ime,
			@RequestParam(required = false) String eMail,
			@RequestParam(required = false) String telefon,
			HttpSession session) throws IOException {
		System.out.println(frizerId);
		System.out.println(datumIVreme);
		System.out.println(ime);
		System.out.println(eMail);
		System.out.println(telefon);
		
		Map<Long, Termini> terminiMap = (Map<Long, Termini>) servletContext.getAttribute(TERMINI_KEY);
		Map<Integer, Frizeri> frizeriMap = (Map<Integer, Frizeri>) servletContext.getAttribute(FRIZERI_KEY);
		Frizeri frizeri = null;
		
		for (Frizeri it : frizeriMap.values()) {
			if(it.getIme().equals(frizerId)) {
				frizeri = it;
				break;
			}
		}
		LocalDateTime datum = LocalDateTime.parse(datumIVreme);
		if (frizerId == "" && datumIVreme == "" && ime == "" && eMail == "" && telefon == "") {
			System.out.println("Nisu sva polja popunjena");
			return "zadatak2";
		} else if(datum.isBefore(LocalDateTime.now())) {
			System.out.println("Datum:" + datum + " je pre dananje:" + LocalDateTime.now());
			return "zadatak2";
//		} else if() {}
//			
		}
		
		if(session.getAttribute(KORISNIK_KEY) == null) {//posto mi ne kreira sesiju, ovde radim rucno
			Double initIznos = 1000.00;
			session.setAttribute(KORISNIK_KEY, initIznos);
		}
		Double trenutniIznos = (Double) session.getAttribute(KORISNIK_KEY);// zadatak4
		Double umenjenIznos = trenutniIznos - frizeri.getCenaSisanja();
		session.setAttribute(TerminiController.KORISNIK_KEY, umenjenIznos);
		
		long id = terminiMap.values().size() + 1;
		Termini termin = new Termini(id, frizeri, datum, ime, eMail, telefon);
		terminiMap.put(id, termin);
		
		
		for (Termini it : terminiMap.values()) {
			System.out.println(it);
		}
		System.out.println(session.getAttribute(KORISNIK_KEY));
		
		return "zadatak2";
	}
}
