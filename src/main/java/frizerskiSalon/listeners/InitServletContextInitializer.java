package frizerskiSalon.listeners;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.stereotype.Component;

import frizerskiSalon.controller.TerminiController;
import frizerskiSalon.model.Frizeri;
import frizerskiSalon.model.Termini;


@Component
public final class InitServletContextInitializer implements ServletContextInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
    	System.out.println("Inicijalizacija konteksta pri ServletContextInitializer...");
    	
    	
    	Map<Integer, Frizeri> frizeriMap = new LinkedHashMap<>();
    	frizeriMap.put(1, new Frizeri(1, "Frizer 1", 600.0, "MONDAY,WEDNESDAY,FRIDAY", 1));
    	frizeriMap.put(2, new Frizeri(2, "Frizer 2", 600.0, "MONDAY,WEDNESDAY,FRIDAY", 2));
    	frizeriMap.put(3, new Frizeri(3, "Frizer 3", 700.0, "TUESDAY,THURSDAY,SATURDAY", 1));
    	frizeriMap.put(4, new Frizeri(3, "Frizer 4", 700.0, "TUESDAY,THURSDAY,SATURDAY", 2));
    	servletContext.setAttribute(TerminiController.FRIZERI_KEY, frizeriMap);
    	
    	
    	Map<Long, Termini> terminiMap = new LinkedHashMap<>();
    	terminiMap.put(1L, new Termini(1L, frizeriMap.get(1), LocalDateTime.parse("2023-11-01 09:00", TerminiController.FORMATER), "Aaa", "a@a.com", "+381611111111"));
    	terminiMap.put(2L, new Termini(2L, frizeriMap.get(2), LocalDateTime.parse("2023-11-01 14:00", TerminiController.FORMATER), "Bbb", "b@b.com", "+381622222222"));
    	terminiMap.put(3L, new Termini(3L, frizeriMap.get(1), LocalDateTime.parse("2023-11-01 09:30", TerminiController.FORMATER), "Ccc", "c@c.com", "+381633333333"));
    	terminiMap.put(4L, new Termini(4L, frizeriMap.get(2), LocalDateTime.parse("2023-11-01 14:30", TerminiController.FORMATER), "Ddd", "d@d.com", "+381644444444"));
    	terminiMap.put(5L, new Termini(5L, frizeriMap.get(3), LocalDateTime.parse("2023-11-02 09:00", TerminiController.FORMATER), "Eee", "e@e.com", "+381655555555"));
    	servletContext.setAttribute(TerminiController.TERMINI_KEY, terminiMap);
    	
    }

}
